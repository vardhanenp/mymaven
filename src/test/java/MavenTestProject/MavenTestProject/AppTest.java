package MavenTestProject.MavenTestProject;
import java.util.concurrent.TimeUnit;  
/*import junit.framework.Test;*/
import junit.framework.TestCase;
/*import junit.framework.TestSuite;*/
import java.io.FileWriter;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.AfterTest;  
import org.testng.annotations.BeforeTest;  
import org.testng.annotations.Test;  

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
	
	public String baseUrl = "https://www.javatpoint.com/";  
	String driverPath = "D://ChromeExe//chromedriver.exe";  
	public WebDriver driver ;   
	
	@Test             
	public void test() throws IOException {      
	// set the system property for Chrome driver      
	System.setProperty("webdriver.chrome.driver", driverPath);  
	// Create driver object for CHROME browser  
	
	driver = new ChromeDriver();  
	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);  
	driver.manage().window().maximize();  
	driver.get(baseUrl);  
	// get the current URL of the page  
	String URL= driver.getCurrentUrl();  
	System.out.print(URL);  
	//get the title of the page  
	String title = driver.getTitle();                  
	System.out.println(title);  
	 FileWriter fw=new FileWriter("D:\\testout.txt");    
     fw.write("success " + title);  
     fw.close();    
	}     
	
	@Test  
	public void sum() {  
		
	System.out.print("Sum method");  
	int p=10;  
	int q=20;  
	Assert.assertEquals(30, p+q);  
	}  
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    //public static Test suite()
    //{
        //return new TestSuite( AppTest.class );
    //}

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {
        assertTrue( true );
    }
    
  
    public void testgetHelloWorld()
    {
    	assertEquals("hello world", App.getHelloWorld());
    }
    
    @BeforeTest  
    public void beforeTest() {    
    	System.out.println("before test");  
    }     
    @AfterTest  
    public void afterTest() {  
    	driver.quit();  
    	System.out.println("after test");  
    }         
}
